package token;

import java.util.Random;

public class Calculator {
	
	private String buffer, ipAdress, nextIp;
	private static final String mesajDefault = "mesaj de test";

	public Calculator() {
		super();
		this.buffer = "";
		this.ipAdress = generareIpAdress();
		this.nextIp = "";
	}

	public String getNextIp() {
		return nextIp;
	}

	public void setNextIp(String nextIp) {
		this.nextIp = nextIp;
	}

	public String getBuffer() {
		return buffer;
	}

	public void setBuffer(String buffer) {
		this.buffer = buffer;
	}

	public String getIpAdress() {
		return ipAdress;
	}

	public void setIpAdress(String ipAdress) {
		this.ipAdress = ipAdress;
	}
	
	private String generareIpAdress() {
		
		int random;
		
		Random randomGenerator = new Random();
		
		random = randomGenerator.nextInt(256);
		ipAdress = random + ".";
		random = randomGenerator.nextInt(256);
		ipAdress = ipAdress + random;
		random = randomGenerator.nextInt(256);
		ipAdress = ipAdress + "." + random;
		random = randomGenerator.nextInt(256);
		ipAdress = ipAdress + "." + random;
		random = randomGenerator.nextInt(256);
		
		return ipAdress;
	}

	public void incarcaJeton(Jeton jeton) {
		jeton.setOcupare(true);
		jeton.setMesaj(mesajDefault);
		jeton.setIpAdressSursa(this.ipAdress);
	}
	
	public void descarcaJeton(Jeton jeton) {
		jeton.setMesaj("");
		jeton.setSosire(true);
		jeton.setOcupare(false);
	}

}
