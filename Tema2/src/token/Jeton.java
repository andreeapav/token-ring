package token;

public class Jeton {
	
	private String ipAdressSursa, ipAdressDestinatie, mesaj;
	private boolean ocupare, sosire;
	
	public Jeton() {
		super();
		this.mesaj = "";
		this.ocupare = false;
		this.sosire = false;
	}
	public String getIpAdressSursa() {
		return ipAdressSursa;
	}
	public void setIpAdressSursa(String ipAdressSursa) {
		this.ipAdressSursa = ipAdressSursa;
	}
	public String getIpAdressDestinatie() {
		return ipAdressDestinatie;
	}
	public void setIpAdressDestinatie(String ipAdreesDestinatie) {
		this.ipAdressDestinatie = ipAdreesDestinatie;
	}
	public String getMesaj() {
		return mesaj;
	}
	public void setMesaj(String mesaj) {
		this.mesaj = mesaj;
	}
	public boolean isOcupare() {
		return ocupare;
	}
	public void setOcupare(boolean ocupare) {
		this.ocupare = ocupare;
	}
	public boolean isSosire() {
		return sosire;
	}
	public void setSosire(boolean sosire) {
		this.sosire = sosire;
	}

}
