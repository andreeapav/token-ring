package retea;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import token.Calculator;
import token.Jeton;

public class Test {

	public static void main(String[] args) {

		List<Calculator> retea = generareCalculatoare();
		Jeton jeton = new Jeton();
		afisareCalculatoare(0, 10, retea);

		int pozitie = generareSursa();
		int dest = generareDestinatie(pozitie);
		Calculator calculatorSursa = retea.get(pozitie);
		calculatorSursa.setBuffer(jeton.getMesaj());
		Calculator calculatorDestinatie = retea.get(dest);
		System.out.println("Sursa: " + "C" + pozitie + " Destinatia: " + "C" + dest);
		calculatorSursa.incarcaJeton(jeton);
		jeton.setIpAdressDestinatie(calculatorDestinatie.getIpAdress());
		System.out.println("C" + pozitie + ": Muta jetonul");
		pozitie++;
		if (pozitie == 10)
			pozitie = 0;
		
		while (true) {
			Calculator calculatorCurent = retea.get(pozitie);
			if (jeton.getIpAdressDestinatie() != calculatorCurent.getIpAdress()) {
				System.out.println("C" + pozitie + ": Muta jetonul");
				pozitie++;
				if (pozitie == 10)
					pozitie = 0;
			} else {
				calculatorCurent.descarcaJeton(jeton);
				System.out.println("C" + pozitie + ": Am ajuns la destinatie");
				calculatorCurent.setBuffer(jeton.getMesaj());
			}

			while (jeton.getIpAdressSursa() != calculatorSursa.getIpAdress()) {
				System.out.println("C" + pozitie + ": Muta jetonul");
				pozitie++;
				if (pozitie == 10)
					pozitie = 0;
			}
			System.out.println("C" + pozitie + ": Am ajuns inapoi");
			afisareCalculatoare(0, 10, retea);

			pozitie = generareSursa();
			dest = generareDestinatie(pozitie);
			calculatorSursa = retea.get(pozitie);
			calculatorSursa.setBuffer(jeton.getMesaj());
			calculatorDestinatie = retea.get(dest);
			System.out.println("Sursa: " + "C" + pozitie + " Destinatia: " + "C" + dest);
			calculatorSursa.incarcaJeton(jeton);
			jeton.setIpAdressDestinatie(calculatorDestinatie.getIpAdress());
			System.out.println("C" + pozitie + ": Muta jetonul");
			pozitie++;
			if (pozitie == 10)
				pozitie = 0;
		}

	}

	public static void afisareCalculatoare(int poz, int sursa, List<Calculator> retea) {
		for (int i = poz; i < sursa; i++) {
			System.out.print("C" + i + "(" + retea.get(i).getIpAdress() + ") -> null ");
			System.out.println(retea.get(i).getBuffer());
		}
	}

	public static int generareSursa() {

		int sursa;
		Random randomGenerator = new Random();
		sursa = randomGenerator.nextInt(9);

		return sursa;
	}

	public static int generareDestinatie(int sursa) {
		int destinatie;
		Random randomGenerator = new Random();
		destinatie = randomGenerator.nextInt(9);

		while (destinatie == sursa) {
			destinatie = randomGenerator.nextInt(9);
		}

		return destinatie;
	}

	public static List<Calculator> generareCalculatoare() {
		List<Calculator> retea = new ArrayList<>();

		Calculator calculator1 = new Calculator();

		retea.add(calculator1);

		for (int i = 1; i < 10; i++) {
			Calculator calculator = new Calculator();
			retea.add(calculator);
			retea.get(i - 1).setNextIp(retea.get(i).getNextIp());
		}

		retea.get(9).setNextIp(calculator1.getNextIp());

		return retea;
	}
}
